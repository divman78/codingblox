import database.InMemoryDb;
import services.contestService;
import services.userService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Driver {

    public static void main(String[] args) {
        InMemoryDb db = new InMemoryDb();
        userService userService = new userService(db);
        contestService contestService = new contestService(db, userService);

        String buffer;
        Scanner sc = new Scanner(System.in);
        while(true){
            buffer = sc.nextLine();
            System.out.println(buffer);
            var bufferList = buffer.split(" ");
            if(bufferList[0].equals("CreateContest")){
                System.out.println("hi");
                int numOfProblems = Integer.parseInt(bufferList[1]);
                List<Integer> listDifficultyLevel = new ArrayList<Integer>();
                for (int  i = 2; i < bufferList.length; i++){
                    listDifficultyLevel.add(Integer.parseInt(bufferList[i]));
                }
                contestService.createContest(numOfProblems, listDifficultyLevel);
            }
            if(bufferList[0].equals("CreateUser")){
                userService.createUser(bufferList[1]);
            }

            if(bufferList[0].equals("AttendContest")){
                int contestId = Integer.parseInt(bufferList[1]);
                List<String> listDifficultyLevel = new ArrayList<String>();
                for (int  i = 2; i < bufferList.length; i++){
                    listDifficultyLevel.add(bufferList[i]);
                }
                contestService.attendContest(contestId, listDifficultyLevel);
            }
            if(bufferList[0].equals("LeaderBoard")){
                userService.showLeaderBoard();
            }
            if(bufferList[0].equals("ContestHistory")){
                contestService.contestHistory(Integer.parseInt(bufferList[1]));
            }
        }
    }
}
