package model;

import java.util.ArrayList;
import java.util.List;

public class Contest {
    private long contestId;
    private int numOfProblems;
    private List<Integer> problemDifficultyPoints;
    private List<User> contestUsers = new ArrayList<>();

    static int numOfContestsSoFar = 0;

    public Contest(int numOfProblems, List<Integer> problemDifficultyPoints) {
        this.numOfProblems = numOfProblems;
        this.problemDifficultyPoints = problemDifficultyPoints;
        contestId = ++numOfContestsSoFar;
    }

    public long getContestId() {
        return contestId;
    }

    public int getNumOfProblems() {
        return numOfProblems;
    }

    public List<Integer> getProblemDifficultyPoints() {
        return problemDifficultyPoints;
    }

    public List<User> getContestUsers() {
        return contestUsers;
    }

    public void setNumOfProblems(int numOfProblems) {
        this.numOfProblems = numOfProblems;
    }

    public void setProblemDifficultyPoints(List<Integer> problemDifficultyPoints) {
        this.problemDifficultyPoints = problemDifficultyPoints;
    }

    public void setContestUsers(List<User> contestUsers) {
        this.contestUsers = contestUsers;
    }

    @Override
    public String toString() {
        return "Contest{" +
                "contestId=" + contestId +
                ", numOfProblems=" + numOfProblems +
                ", problemDifficultyPoints=" + problemDifficultyPoints +
                ", contestUsers=" + contestUsers +
                '}';
    }
}
