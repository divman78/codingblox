package model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class User {
    private UUID userId;
    private String userName;
    private int rating;
    List<Contest> userContests = new ArrayList<>();

    static int DEFAULT_CONTEST_RATING = 1500;

    public User(String userName) {
        this.userId = UUID.randomUUID();
        this.userName = userName;
        rating = DEFAULT_CONTEST_RATING;
    }

    public UUID getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public int getRating() {
        return rating;
    }

    public List<Contest> getUserContests() {
        return userContests;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setUserContests(List<Contest> userContests) {
        this.userContests = userContests;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", rating=" + rating +
                ", userContests=" + userContests +
                '}';
    }
}
