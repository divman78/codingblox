package database;

import model.Contest;
import model.User;

import java.util.ArrayList;
import java.util.List;

public class InMemoryDb {

    private List<User> users = new ArrayList<User>();
    private List<Contest> contests = new ArrayList<Contest>();

    public List<User> getUsers() {
        return users;
    }

    public List<Contest> getContests() {
        return contests;
    }

    public void addContest(Contest contest){
        contests.add(contest);
    }

    public void addUser(User user){
        users.add(user);
    }
}
