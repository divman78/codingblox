package services;

import database.InMemoryDb;
import model.Contest;
import model.User;

import java.util.List;
import java.util.Random;

public class contestService {
    InMemoryDb db;
    userService userService;

    public contestService(InMemoryDb db, userService userService) {
        this.db = db;
        this.userService = userService;
    }

    public void createContest(int numOfProblems, List<Integer> problemDifficultyPoints){
        if(validateContest(problemDifficultyPoints) == false){
            System.out.println("Sum of difficulty should be equal to 100");
            return;
        }
        Contest contest = new Contest(numOfProblems, problemDifficultyPoints);
        db.addContest(contest);
    }

    public void contestHistory(int contestId){
        var contest = getContest(contestId);
        System.out.println(contest);
    }
    public void attendContest(long contestId, List<String> userNames){
        Contest contest = getContest(contestId);
        if(contest == null){
            System.out.println("Contest does not exists");
            return;
        }
        for(var userName: userNames){
            var user = getUserByName(userName);
            if(user == null){
                System.out.println("user " +  userName + " not present");
                return;
            }
            Random rand = new Random();
            int score = rand.nextInt(100);
            userService.addContestForUser(user, contest);
            contest.getContestUsers().add(user);
            contest.getScores().add(score);
            user.setRating(user.getRating()+ score -50);
            user.getScores().add(score);
        }
    }

    public User getUserByName(String userName){
        for(var user: db.getUsers()){
            if(user.getUserName().equals(userName)){
                return user;
            }
        }
        return null;
    }
    public Contest getContest(long contestId) {
        var contests = db.getContests();

        for(var contest: contests){
            //System.out.println(contest.getContestId());
            if(contestId == contest.getContestId()){
                return contest;
            }
        }
        return null;
    }

    private boolean validateContest(List<Integer> problemDifficultyPoints) {
        int sumProblemDifficultyPoints = 0;
        for(Integer problemDifficultyPoint: problemDifficultyPoints){
            sumProblemDifficultyPoints += problemDifficultyPoint.intValue();
        }
        if(sumProblemDifficultyPoints != 100){
            return false;
        }
        return true;
    }
}
