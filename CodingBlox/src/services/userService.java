package services;

import database.InMemoryDb;
import model.Contest;
import model.User;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;




public class userService {

    InMemoryDb db;

    public static class SortbyRating implements Comparator<User>
    {
        public int compare(User a, User b)
        {
            return - a.getRating() + b.getRating();
        }
    }


    public userService(InMemoryDb db) {
        this.db = db;
    }

    public void createUser(String userName){
        if(validateUser(userName) == false){
            System.out.println("User Already Exists");
            return;
        }
        User user = new User(userName);
        db.addUser(user);
    }

    public User getUserByName(String userName){
        for(var user: db.getUsers()){
            if(user.getUserName().equals(userName)){
                return user;
            }
        }
        return null;
    }

    public void addContestForUser(User user, Contest contest){
        var userContests = user.getUserContests();
        userContests.add(contest);
    }

    public void showLeaderBoard(){
        var users = db.getUsers();

        Collections.sort(users, new SortbyRating());
        for (int i = 0; i < users.size(); i++){
            System.out.println(users.get(i));
        }

    }

    private boolean validateUser(String userName) {
        List<User> users = db.getUsers();
        for(User user: users){
            if(user.getUserName().equals(userName)){
                return false;
            }
        }
        return true;
    }
}
